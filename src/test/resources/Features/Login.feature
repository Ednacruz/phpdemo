#Liz
Feature: Reserva de hotel
  Se requiere ingresar a la pagina y realizar una reserva de hotel exitosa

  Scenario: Reserva Exitosa
    Given Ingresar a la página de manera exitosa
    And Realizar inicio de sesión con usuario "user@phptravels.com" y clave "demouser"
    And Buscar hotel
    And Consultar detalles
    When Seleccionar una opción y reservarla
    And Llenar datos del formulario y confirmar reserva
    Then Validar que la reserva fue exitosa