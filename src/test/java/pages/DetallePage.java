package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;





public class DetallePage {

	WebDriver driver;
	public DetallePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}	
	
    By btnBookNow = By.xpath("//*[@id=\"availability\"]/div[2]/div[1]/div[2]/div/div[2]/form/div/div[4]/div/div/button");
	
	
	public void hotelDetalles() {
		
		driver.findElement(btnBookNow).sendKeys(Keys.PAGE_DOWN);
		driver.findElement(btnBookNow).sendKeys(Keys.DOWN);
	    driver.findElement(btnBookNow).click();
		
	}


}
