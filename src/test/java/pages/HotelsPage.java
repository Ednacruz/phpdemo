package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HotelsPage {

	WebDriver driver;
	
	public HotelsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
}
	
	By cboSearch = By.xpath("//span[contains(text(),'Search by City')]//parent::span[1]");
	By txtSearch = By.xpath("//input[@type='search' and @role='searchbox']");
	By cboResult = By.xpath("//*[@class=\"select2-results__option select2-results__option--highlighted\"]");
	By cboCheckin = By.xpath("//input[@name='checkin']");
	By cboCheckout = By.xpath("//input[@name='checkout']");
	By btnNext = By.xpath("//th[@class='next'][1]");
	By btnDayIn = By.xpath("//td[@class='day ' and contains(text(),'28')][1]");
	By btnDayOut = By.xpath("//*[@id=\"fadein\"]/div[3]/div[1]/table/tbody/tr[5]/td[7]");
	By ddmTravellers = By.xpath("//*[@class='dropdown dropdown-contain']/a");	
    By btnSearch = By.xpath("//*[@class='btn-search text-center']/button");
    By btnDetalles = By.xpath("//*[@id=\"swissotel le plaza basel\"]/div/div[2]/div/div[2]/div/a");


	public void buscarHotel () {
	
    	
       	driver.findElement(cboSearch).click();
    	driver.findElement(txtSearch).clear();
		driver.findElement(txtSearch).sendKeys("Singapore");
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(cboResult));
    	driver.findElement(cboResult).click();
	    driver.findElement(cboCheckin).click();
	  	driver.findElement(btnNext).click();
		driver.findElement(btnNext).click();
		driver.findElement(btnNext).click();
		driver.findElement(btnDayIn).click();
		driver.findElement(btnDayOut).click();
		driver.findElement(ddmTravellers).click();	
		driver.findElement(ddmTravellers).click();
		driver.findElement(btnSearch).click();
		
		
		
		
				
		}


	public void verDetalles() {
	
		driver.findElement(btnDetalles).click();
		
	}
}
