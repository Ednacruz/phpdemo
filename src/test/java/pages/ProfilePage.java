package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class ProfilePage {

	WebDriver driver;
	public ProfilePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

}
	
	By lblUsuario = By.xpath("//*[contains(text(),'Demo')][1]");
	By btnHotels = By.xpath("//a[@title='home' and contains(text(),'Hotels')]");

	public void verificaUsuario() {	
		driver.findElement(lblUsuario).isDisplayed();
		
		
		
	}

	public void menuHotel() {
		driver.findElement(btnHotels).click();
		
	}
	
	
	
	
}
