package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


public class InformationPage {

	WebDriver driver;
	public InformationPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	
}
	
	By cboNationality = By.xpath("//option[@value='CO']");
	By cboTitle1 = By.xpath("//select[@name='title_1']//option[@value='Miss']");
	By txtFirstName1 = By.xpath("//input[@name='firstname_1']");
	By txtLastName1 = By.xpath("//input[@name='lastname_1']");
	By cboTitle2 = By.xpath("//select[@name='title_2']//option[@value='Mr']");
	By txtFirstName2 = By.xpath("//input[@name='firstname_2']");
	By txtLastName2 = By.xpath("//input[@name='lastname_2']");
	By chkPayLater = By.xpath("//*[@value='pay-later']");
	By chkAgree = By.xpath("//*[@id=\"fadein\"]/div[2]/form/section/div/div/div[1]/div[4]/div/div/div");
	By btnConfirm = By.xpath("//button[@id='booking']");
	
	
	

	
	public void llenarFormulario() throws InterruptedException {
		
		driver.findElement(cboNationality).click();
		driver.findElement(cboTitle1).click();
		driver.findElement(txtFirstName1).click();
		driver.findElement(txtFirstName1).clear();
		driver.findElement(txtFirstName1).sendKeys("Sofia");
		driver.findElement(txtLastName1).click();
		driver.findElement(txtLastName1).clear();
		driver.findElement(txtLastName1).sendKeys("Castro");
		driver.findElement(cboTitle2).click();
		driver.findElement(txtFirstName2).click();
		driver.findElement(txtFirstName2).clear();
		driver.findElement(txtFirstName2).sendKeys("Carlos");
		driver.findElement(txtLastName2).click();
		driver.findElement(txtLastName2).clear();
		driver.findElement(txtLastName2).sendKeys("Sanchez");
		driver.findElement(chkPayLater).sendKeys(Keys.PAGE_DOWN);
		driver.findElement(chkPayLater).click();
		driver.findElement(chkPayLater).sendKeys(Keys.PAGE_DOWN);
  		Thread.sleep(10000);
    	driver.findElement(chkAgree).click();
		driver.findElement(btnConfirm).click();
		
		}
	
}
