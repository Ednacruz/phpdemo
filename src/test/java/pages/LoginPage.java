package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class LoginPage  {

	WebDriver driver;
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	By btnCookies = By.xpath("//button[contains(text(),'Got It')]");
	By txtUsuario = By.xpath("//*[@placeholder='Email']");
	By txtClave = By.xpath("//*[@placeholder='Password']");
	By btnLogin = By.xpath("//span[contains(text(),'Login')]//parent::button");
	
	


	public void iniciarSesion(String usuario, String clave) {
	
    	driver.findElement(btnCookies).click();
		driver.findElement(txtUsuario).sendKeys(usuario);
		driver.findElement(txtClave).sendKeys(clave);
		driver.findElement(btnLogin).click();
				
		}

	
		
}

