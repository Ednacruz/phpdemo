package pages;

import static org.junit.Assert.assertTrue;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;




public class PhpTravelsPage {

	WebDriver driver;
	
	public PhpTravelsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	By mensajeInicio = By.xpath("//*[contains(text(),'Application Test Drive')]");
	By btnCustomerFrontEdn = By.xpath("//a[contains(text(),'Customer - Front-End')]");

	
	
	public void verificaHome() {
		assertTrue(driver.findElement(mensajeInicio).isDisplayed());
    	driver.findElement(btnCustomerFrontEdn).click();
    	String mainTab = driver.getWindowHandle();
    	System.out.println("MainTab:" + mainTab);
    	
    	Set<String> handles = driver.getWindowHandles();
    	
    	for (String actual : handles) {
    		System.out.println("--Handled ID:" + actual);
    		if(!actual.equalsIgnoreCase(mainTab)) {
    			driver.switchTo().window(actual);
       		}
      	}
   		
   	}

	

	
	
   
}
