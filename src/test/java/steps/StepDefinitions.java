package steps;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.DetallePage;
import pages.HotelsPage;
import pages.InformationPage;
import pages.LoginPage;
import pages.PhpTravelsPage;
import pages.ProfilePage;

public class StepDefinitions {

	WebDriver driver;
	
	@Before
	public void before() {
		System.setProperty("webdriver.chrome.driver","./src/test/resources/Utils/Chromedriver.exe");
		driver = new  ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://phptravels.com/demo");
		
//		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	
	}
	
	@Given("Ingresar a la página de manera exitosa")
	public void ingresar_a_la_página_de_manera_exitosa() throws InterruptedException {
		PhpTravelsPage phpTravelsPage = new PhpTravelsPage(driver);
		phpTravelsPage.verificaHome();
	
	}
	
	@And("Realizar inicio de sesión con usuario {string} y clave {string}")
	public void realizar_inicio_de_sesión_con_usuario_y_clave(String usuario, String clave) {
		LoginPage loginPage = new LoginPage(driver);
		loginPage.iniciarSesion(usuario, clave);
		ProfilePage profilePage = new ProfilePage(driver);
		profilePage.verificaUsuario();
	}
	@And("Buscar hotel")
	public void buscar_hotel()  {
		ProfilePage profilePage = new ProfilePage(driver);
		profilePage.menuHotel();
		HotelsPage hotelPage = new HotelsPage(driver);
		hotelPage.buscarHotel();
		
	}
	@And("Consultar detalles")
	public void consultar_detalles() {
		HotelsPage hotelPage = new HotelsPage(driver);
		hotelPage.verDetalles();
		
	}
	@When("Seleccionar una opción y reservarla")
	public void seleccionar_una_opción_y_reservarla() {
		DetallePage detallePage = new DetallePage(driver);
		detallePage.hotelDetalles();
		
	}
	@And("Llenar datos del formulario y confirmar reserva")
	public void llenar_datos_del_formulario_y_confirmar_reserva() throws InterruptedException {
		InformationPage informationPage = new InformationPage(driver);
		informationPage.llenarFormulario();
   
	}
	@Then("Validar que la reserva fue exitosa")
	public void validar_que_la_reserva_fue_exitosa() {
    
	}

}

